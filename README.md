# For Loop #
 Loops are used to execute a set of statements repeatedly until a particular condition is satisfied. In Java we have three types of basic loops: for, while and do-while. In this tutorial we will learn how to use “for loop” in Java.
 
 https://beginnersbook.com/2015/03/for-loop-in-java-with-example/