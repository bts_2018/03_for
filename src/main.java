import java.util.Arrays;

public class main {

	public static void main(String[] args) {

		//case 1 
		for(int i=0; i<=10; i++){
			System.out.println("The value of i is: "+i);
		}

		//case 2
		System.out.println();
		int arr[]={2,11,45,9,4,2,9,0};
		for(int i=0; i<arr.length; i++){

			System.out.println("The value of i is: "+ arr[i]);
		}


		//case 3
		System.out.println();
		String[] cities = new String[]{"Milan", "Barcelona", "London","Paris"};
		for(String city : cities) {
			System.out.println("The value is:" +  city);
		}



	}

}
